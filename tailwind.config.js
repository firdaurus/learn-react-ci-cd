module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'primary': '#0D28A6',
        'red': '#D00C1A',
        'abu' : '#F4F5F7',
        'danger' : '#FA2C5A',
        'success' : '#73CA5C',
        'slate' : '#F1F3FF',
        'darkBlue' : {
          1 : '#CFD4ED',
          2 : '#AEB7E1',
          4 : '#0D28A6',
        },
        'neutral' : {
          1 : '#FFFFFF',
          2 : '#D0D0D0',
          3 : '#8A8A8A',
          4: '#3C3C3C',
        },
        'limeGreen' : {
          1: '#DEF1DF',
          2: '#C9E7CA',
          4: '#5CB85F',
        },
      },
    },
  },
  plugins: [],
}
