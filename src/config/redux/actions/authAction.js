import { signInWithPopup, GoogleAuthProvider } from "firebase/auth";
import { authentication } from "./../../firebase/firebaseConfig";


export const LoginGoogle = () => {
  return async (dispatch) => {
    const data = await signInWithPopup(
      authentication,
      new GoogleAuthProvider()
    );
    dispatch({ type: "SET_DATA_LOGIN", payload: data });
    return data;
  };
};

export const LoginEmail = (email, password) => {
  return async (dispatch) => {
    await new Promise(resolve => setTimeout(resolve, 1000)); //Fake API Delay
    dispatch({ type: "SET_DATA_LOGIN", payload: { email, password } });
    return true; //success
  };
};

export const Logout = () => {
    return (dispatch) => {
        dispatch({ type: "SET_DATA_LOGIN", payload: null });
    };
};

export const Register = (email, password, callback) => {
    return async (dispatch) => {
        await new Promise(resolve => setTimeout(resolve, 1000)); //Fake API Delay
        callback();       
    };
}