const initialState = {
    selectedCar: {},
    cars: [],
};

const dataReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_SELECTED_CAR':
            return {
                ...state,
                selectedCar: action.payload,
            };
        case 'SET_CARS':
            return {
                ...state,
                cars: action.payload,
            };
        default:
            return state;
    }
}

export default dataReducer;