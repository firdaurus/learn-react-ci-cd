import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { Register as RegisterAction } from "../../config/redux/actions/authAction";
import gambar from "../../assets/bg.png";
import { toast } from "react-toastify";

export default function Register() {

	const [ email, setEmail ] = useState("");
	const [ password, setPassword ] = useState("");
	const navigate = useNavigate();
	const dispatch = useDispatch();

	const handleSubmit = async (e) => {
		e.preventDefault()
		await dispatch(RegisterAction(email, password, () => {
			toast.success("Register Success, Please Login!", {
				position: "bottom-right",
				autoClose: 2000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true,
				progress: undefined,
				})
			navigate("/login")
		}));
	};

  return (
	<div>
	  <div className="flex w-screen h-screen">
		<img className="flex-grow object-cover" src={gambar} alt="Background" />
		<div className="w-1/3 p-10 flex items-center justify-center">
		  <div className="w-full flex flex-col gap-y-5">
			<div className="bg-gray w-24 h-8"></div>
			<p className="font-bold text-2xl">Create, new Account</p>
			<div
			  id="alert"
			  className="text-sm w-full mt-2 bg-pink text-red rounded-md py-4 px-8 bg-red bg-opacity-10 hidden"
			></div>
			{/* form sign-in */}
			<form
			  className="flex flex-col font-light"
			  id="sign-in-form"
			  onSubmit={handleSubmit}
			>
			  <p>Email</p>
			  <input
				type="email"
				id="email"
				value={email}
				onChange={(e) => setEmail(e.target.value)}
				className="font-md w-full mt-2 mb-4 border border-gray-300 rounded-md p-2 outline-none"
				placeholder="Contoh: johndee@gmail.com"
			  />
			  <p>Password</p>
			  <input
				type="password"
				id="password"
				value={password}
				onChange={(e) => setPassword(e.target.value)}
				className="text-sm w-full mt-2 mb-7 border border-gray-300 rounded-md p-2 outline-none"
				placeholder="6+ karakter"
			  />
			  <button className="bg-primary text-white font-bold py-2 px-4 rounded-md">
				Sign Up
			  </button>
			  <p className="mt-3 text-center">
				Already have an account? <Link to="/login" className="text-primary font-semibold">Login</Link>
			  </p>
			</form>
		  </div>
		</div>
	  </div>
	</div>
  );
}
