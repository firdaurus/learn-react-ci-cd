import React, { useState } from 'react';
import NavbarBlue from '../../../components/AdminComponents/navbarBlue';
import NavbarWhite from '../../../components/AdminComponents/navbarWhite';
import Header from '../../../components/AdminComponents/header';
import { ChevronRight, Upload } from 'react-feather';
import { useNavigate, Link } from 'react-router-dom';
import { toast } from 'react-toastify'

export default function AddCar() {

    const navigate = useNavigate();

    const [isLoading, setLoading] = useState(false)

    const handleSubmit = async () => {
        setLoading(true)
        await new Promise(resolve => setTimeout(resolve, 1000))
        setLoading(false)
        toast.success("Mobil berhasil ditambahkan!", {
            position: "top-center",
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        })
        navigate('/list-car', { replace: true })
    }

  return (
    <div className="w-screen h-screen flex">
        <NavbarBlue />

        <div className="flex flex-col flex-grow h-screen">
            <Header />

            <div className="flex z-0 flex-[1_1_auto] overflow-y-auto">
                <NavbarWhite />

                {/* Main Content */}
                <div id="addCar" className="bg-abu w-full flex flex-col gap-5 p-5 overflow-y-scroll">
                    <div className="flex gap-1 items-center">
                        <span className="font-bold text-xs">Cars</span>
                        <ChevronRight height="1rem" stroke-width="3" />
                        <span className="font-bold text-xs">List Car</span>
                        <ChevronRight height="1rem" stroke-width="3" />
                        <span className="font-light text-xs">Add New Car</span>
                    </div>
                    <h1 className="font-bold text-2xl">Add New Car</h1>
                    <div className="bg-white py-4 px-3 text-sm">
                        <table>
                            <tr>
                                <td className="w-48">Nama<sup className="text-red">*</sup></td>
                                <td>
                                    <input type="text" className="p-2 w-72 my-2 border border-neutral-2" placeholder="Placeholder" />
                                </td>
                            </tr>
                            <tr>
                                <td>Harga <sup className="text-red">*</sup></td>
                                <td>
                                    <input type="text" className="p-2 w-72 my-2 border border-neutral-2" placeholder="Placeholder" />
                                </td>
                            </tr>
                            <tr>
                                <td>Foto <sup className="text-red">*</sup></td>
                                <td className="relative flex flex-col py-2">
                                    <input type="text" className="p-2 w-72 border border-neutral-2" placeholder="Placeholder" />
                                    <button className="absolute top-2 right-3 mt-2 text-neutral-3">
                                        <Upload />
                                    </button>
                                    <p className="text-neutral-3 text-xs mt-1">File size max. 2MB</p>
                                </td>
                            </tr>
                            <tr>
                                <td className="py-2">Start Rent</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td className="py-2">Finish Rent</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td className="py-2">Created at</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td className="py-2">Updated at</td>
                                <td>-</td>
                            </tr>
                        </table>
                    </div>
                    <div className="flex gap-4">
                        <button disabled={isLoading} className="py-2 px-3 bg-white text-darkBlue-4 border-2 border-darkBlue-4 font-semibold rounded-sm disabled:opacity-50">
                            <Link replace to="/list-car" >
                                    Cancel
                            </Link>
                        </button>
                        <button onClick={handleSubmit} disabled={isLoading} className="py-2 px-3 bg-darkBlue-4 text-white border-2 border-darkBlue-4 font-semibold rounded-sm disabled:opacity-50">{isLoading ? "Menyimpan..." : "Save"}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    );
};
