import React, { useState } from 'react';
import DeletePopUp from '../../../components/AdminComponents/deletePopUp';
import NavbarBlue from '../../../components/AdminComponents/navbarBlue';
import NavbarWhite from '../../../components/AdminComponents/navbarWhite';
import Header from '../../../components/AdminComponents/header';
import Alert from '../../../components/AdminComponents/alert';
import { ChevronRight, Plus, Key, Clock, Trash2, Edit } from 'react-feather';
import car from '../../../assets/car2.png';
import { Link } from 'react-router-dom';

export default function ListCar() {
    
    const [ isShowDeletePopup, setShowDeletePopup] = useState(false);
    const [ deleteId, setDeleteId ] = useState(0);

    const showDeletePopup = (id) => {
        setShowDeletePopup(true);
        setDeleteId(id);
    }

  return (
      <>
        <div className="w-screen h-screen flex">
            <DeletePopUp show={isShowDeletePopup} id={deleteId} onClose={() => setShowDeletePopup(false)} />
            <NavbarBlue />
    
            <div className="flex flex-col flex-grow h-screen">
                <Header />
    
                <div className="flex z-0 flex-[1_1_auto] overflow-y-auto">
                    <NavbarWhite />
    
                     {/* <!-- main content --> */}
                    <div className="bg-abu w-full flex flex-col gap-5 p-5 overflow-y-scroll relative">
                        <Alert />
            
                        <div className="flex gap-1 items-center">
                            <span className="font-bold text-xs">Cars</span>
                            <ChevronRight height="1rem" stroke-width="3" />
                            <span className="font-light text-xs">List Car</span>
                        </div>
                        <div className="flex justify-between">
                            <span className="font-bold text-2xl">List Car</span>
                            <Link to="/add-car" className="bg-primary p-2 flex gap-3 text-sm font-bold text-white items-center rounded-sm">
                                <Plus height="1.25rem" />
                                <span>Add New Car</span>
                            </Link>
                        </div>
                        {/* <!-- filter --> */}
                        <div className="flex gap-3 text-sm font-bold">
                        <span className="bg-darkBlue-1 px-2 py-1 border border-darkBlue-4 rounded-sm text-darkBlue-4">All</span>
                        <span className="bg-white px-2 py-1 border border-darkBlue-2 rounded-sm text-darkBlue-2">Small</span>
                        <span className="bg-white px-2 py-1 border border-darkBlue-2 rounded-sm text-darkBlue-2">Medium</span>
                        <span className="bg-white px-2 py-1 border border-darkBlue-2 rounded-sm text-darkBlue-2">Large</span>
                        </div>
                        {/* <!-- list car --> */}
                        <div className="grid grid-cols-3 gap-5">
                        {
                            [...Array(9)].map((_, i) => (
                                <div className="flex items-center justify-center bg-white rounded-md shadow-md p-5">
                                    <div className="flex flex-col gap-3 text-sm">
                                    <img src={car} alt="Gambar mobil" className="" />
                                    <p>Nama/Tipe Mobil</p>
                                    <p className="font-bold">Rp 430.000 / hari</p>
                                    <div className="flex gap-1 items-center font-light">
                                        <Key height="1rem" className="text-neutral-3" />
                                        <span>Start rent - Finish rent</span>
                                    </div>
                                    <div className="flex gap-1 items-center font light">
                                        <Clock height="1rem" className="text-neutral-3" />
                                        <span>Updated at 4 Apr 2022, 09.00</span>
                                    </div>
                                    <div className="grid grid-cols-2 gap-3">
                                        <button onClick={() => showDeletePopup(i)} className="flex gap-1 justify-center items-center border border-danger py-2 font-bold text-danger rounded-sm">
                                        <Trash2 height="1rem" stroke-width="3" />
                                        <span>Delete</span>
                                        </button>
                                        <button className="flex gap-1 justify-center items-center py-2 font-bold text-white bg-limeGreen-4 rounded-sm">
                                        <Edit height="1rem" stroke-width="3" />
                                        <span>Edit</span>
                                        </button>
                                    </div>
                                    </div>
                                </div>
                            ))
                        }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </>
  )
}
