import React from 'react';
import NavbarBlue from '../../../components/AdminComponents/navbarBlue';
import Header from '../../../components/AdminComponents/header';
import NavbarWhite from '../../../components/AdminComponents/navbarWhite';
import { ChevronDown, ChevronLeft, ChevronRight } from 'react-feather';

export default function Dashboard() {
  return (
    <div className="w-screen h-screen flex">
        <NavbarBlue />
        <div className="flex flex-col flex-grow h-screen">
            <Header />
            <div className="flex z-0 flex-[1_1_auto] overflow-y-auto">
                <NavbarWhite />

                {/* Main content */}
                <div className="w-full flex flex-col gap-5 p-5 overflow-y-scroll bg-abu">
                    <div className="flex gap-1 items-center">
                        <span className="font-bold text-xs">Dashboard</span>
                        <ChevronRight height="1rem" stroke-width="3" />
                        <span className="font-light text-xs">Dashboard</span>
                    </div>
                    <p className="font-bold text-2xl">DASHBOARD</p>
                    {/* List Order */}
                    <div className="flex gap-1">
                        <span className="h-full w-1 bg-primary"></span>
                        <p className="font-bold">List Order</p>
                    </div>
                    {/* Tabel List Order */}
                    <table id="list-order" className="text-sm text-left">
                        <thead>
                            <tr>
                                <th className="bg-darkBlue-1 font-bold text-black py-2 px-2 relative">
                                    No
                                </th>
                                <th className="bg-darkBlue-1 font-bold text-black py-2 px-2 relative">
                                    User Email
                                    <img src="https://binar-dashboard-challenge-03.herokuapp.com/img/sort.svg" className="absolute right-2 top-2" alt="" />
                                </th>
                                <th className="bg-darkBlue-1 font-bold text-black py-2 px-2 relative">
                                    Car
                                    <img src="https://binar-dashboard-challenge-03.herokuapp.com/img/sort.svg" className="absolute right-2 top-2" alt="" />
                                </th>
                                <th className="bg-darkBlue-1 font-bold text-black py-2 px-2 relative">
                                    Start Rent
                                    <img src="https://binar-dashboard-challenge-03.herokuapp.com/img/sort.svg" className="absolute right-2 top-2" alt="" />
                                </th>
                                <th className="bg-darkBlue-1 font-bold text-black py-2 px-2 relative">
                                    Finish Rent
                                    <img src="https://binar-dashboard-challenge-03.herokuapp.com/img/sort.svg" className="absolute right-2 top-2" alt="" />
                                </th>
                                <th className="bg-darkBlue-1 font-bold text-black py-2 px-2 relative">
                                    Price
                                    <img src="https://binar-dashboard-challenge-03.herokuapp.com/img/sort.svg" className="absolute right-2 top-2" alt="" />
                                </th>
                                <th className="bg-darkBlue-1 font-bold text-black py-2 px-2 relative">
                                    Status
                                    <img src="https://binar-dashboard-challenge-03.herokuapp.com/img/sort.svg" className="absolute right-2 top-2" alt="" />
                                </th>
                            </tr>
                        </thead>
                        <tbody className="bg-white">
                            {
                                [...new Array(10)].map((_, index) => (
                                    <tr>
                                        <td className="pl-4">{index+1}</td>
                                        <td className="py-2 px-2">User Email</td>
                                        <td className="py-2 px-2">Car</td>
                                        <td className="py-2 px-2">Start Rent</td>
                                        <td className="py-2 px-2">Finish Rent</td>
                                        <td className="py-2 px-2">Price</td>
                                        <td className="py-2 px-2">Status</td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>

                    {/* table nav */}
                    <div className="w-full flex justify-between items-end">
                        <div className="flex text-sm gap-4">
                            <div className="flex flex-col gap-3">
                                <p className="text-light">Limit:</p>
                                <div className="bg-white h-10 px-3 border border-neutral-2 rounded-sm flex justify-center items-center text-neutral-3">
                                    <span>10</span>
                                    <ChevronDown height="1rem" />
                                </div>
                            </div>

                            <div className="flex flex-col gap-3">
                                <p className="text-light">Jump to Page:</p>
                                <div className="flex">
                                    <div className="bg-white h-10 px-3 border border-neutral-2 rounded-sm flex justify-center items-center text-neutral-3">
                                        <span>1</span>
                                        <ChevronDown height="1rem" />
                                    </div>
                                    <button className="h-10 px-3 bg-darkBlue-4 text-white font-bold">
                                        Go
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div className="flex text-sm">
                            <div className="bg-white w-8 h-8 border border-neutral-2 grid place-content-center text-neutral-3">
                                <ChevronLeft height="1rem" />
                            </div>
                            <div className="bg-darkBlue-1 w-8 h-8 border border-darkBlue-4 grid place-content-center text-darkBlue-4">
                                <span>1</span>
                            </div>
                            <div className="bg-white w-8 h-8 border border-neutral-2 grid place-content-center text-neutral-3">
                                <span>2</span>
                            </div>
                            <div className="bg-white w-8 h-8 border border-neutral-2 grid place-content-center text-neutral-3">
                                <span>3</span>
                            </div>
                            <div className="bg-white w-8 h-8 border border-neutral-2 grid place-content-center text-neutral-3">
                                <span>...</span>
                            </div>
                            <div className="bg-white w-8 h-8 border border-neutral-2 grid place-content-center text-neutral-3">
                                <span>9</span>
                            </div>
                            <div className="bg-white w-8 h-8 border border-neutral-2 grid place-content-center text-neutral-3">
                                <ChevronRight height="1rem" />
                            </div>
                        </div>
                    </div>
                    
                    {/* List Car */}
                    <div className="flex gap-1 mt-5">
                        <span className="h-full w-1 bg-primary"></span>
                        <p className="font-bold">List Car</p>
                    </div>
                    {/* Tabel List Order */}
                    <table id="list-order" className="text-sm text-left">
                        <thead>
                            <tr>
                                <th className="bg-darkBlue-1 font-bold text-black py-2 px-2 relative">
                                    No
                                </th>
                                <th className="bg-darkBlue-1 font-bold text-black py-2 px-2 relative">
                                    Name
                                    <img src="https://binar-dashboard-challenge-03.herokuapp.com/img/sort.svg" className="absolute right-2 top-2" alt="" />
                                </th>
                                <th className="bg-darkBlue-1 font-bold text-black py-2 px-2 relative">
                                    Category
                                    <img src="https://binar-dashboard-challenge-03.herokuapp.com/img/sort.svg" className="absolute right-2 top-2" alt="" />
                                </th>
                                <th className="bg-darkBlue-1 font-bold text-black py-2 px-2 relative">
                                    Price
                                    <img src="https://binar-dashboard-challenge-03.herokuapp.com/img/sort.svg" className="absolute right-2 top-2" alt="" />
                                </th>
                                <th className="bg-darkBlue-1 font-bold text-black py-2 px-2 relative">
                                    Start Rent
                                    <img src="https://binar-dashboard-challenge-03.herokuapp.com/img/sort.svg" className="absolute right-2 top-2" alt="" />
                                </th>
                                <th className="bg-darkBlue-1 font-bold text-black py-2 px-2 relative">
                                    Finish Rent
                                    <img src="https://binar-dashboard-challenge-03.herokuapp.com/img/sort.svg" className="absolute right-2 top-2" alt="" />
                                </th>
                                <th className="bg-darkBlue-1 font-bold text-black py-2 px-2 relative">
                                    Created at
                                    <img src="https://binar-dashboard-challenge-03.herokuapp.com/img/sort.svg" className="absolute right-2 top-2" alt="" />
                                </th>
                                <th className="bg-darkBlue-1 font-bold text-black py-2 px-2 relative">
                                    Updated at
                                    <img src="https://binar-dashboard-challenge-03.herokuapp.com/img/sort.svg" className="absolute right-2 top-2" alt="" />
                                </th>
                            </tr>
                        </thead>
                        <tbody className="bg-white">
                            {
                                [...new Array(10)].map((_, index) => (
                                    <tr>
                                        <td className="pl-4">{index+1}</td>
                                        <td className="py-2 px-2">Name</td>
                                        <td className="py-2 px-2">Category</td>
                                        <td className="py-2 px-2">Price</td>
                                        <td className="py-2 px-2">{index ===  1 || index === 5 ? '-' : 'Start Rent'}</td>
                                        <td className="py-2 px-2">{index ===  1 || index === 5 ? '-' : 'Finish Rent'}</td>
                                        <td className="py-2 px-2">Created at</td>
                                        <td className="py-2 px-2">Updated at</td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>

                    {/* Table Nav */}
                    <div className="w-full flex justify-between items-end">
                        <div className="flex text-sm gap-4">
                            <div className="flex flex-col gap-3">
                                <p className="text-light">Limit:</p>
                                <div className="bg-white h-10 px-3 border border-neutral-2 rounded-sm flex justify-center items-center text-neutral-3">
                                    <span>50</span>
                                    <ChevronDown height="1rem" />
                                </div>
                            </div>

                            <div className="flex flex-col gap-3">
                                <p className="text-light">Jump to Page:</p>
                                <div className="flex">
                                    <div className="bg-white h-10 px-3 border border-neutral-2 rounded-sm flex justify-center items-center text-neutral-3">
                                        <span>1</span>
                                        <ChevronDown height="1rem" />
                                    </div>
                                    <button className="h-10 px-3 bg-darkBlue-4 text-white font-bold">
                                        Go
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div className="flex text-sm">
                            <div className="bg-white w-8 h-8 border border-neutral-2 grid place-content-center text-neutral-3">
                                <ChevronLeft height="1rem" /> 
                            </div>
                            <div className="bg-darkBlue-1 w-8 h-8 border border-darkBlue-4 grid place-content-center text-darkBlue-4">
                                <span>1</span>
                            </div>
                            <div className="bg-white w-8 h-8 border border-neutral-2 grid place-content-center text-neutral-3">
                                <span>2</span>
                            </div>
                            <div className="bg-white w-8 h-8 border border-neutral-2 grid place-content-center text-neutral-3">
                                <span>3</span>
                            </div>
                            <div className="bg-white w-8 h-8 border border-neutral-2 grid place-content-center text-neutral-3">
                                <span>...</span>
                            </div>
                            <div className="bg-white w-8 h-8 border border-neutral-2 grid place-content-center text-neutral-3">
                                <span>9</span>
                            </div>
                            <div className="bg-white w-8 h-8 border border-neutral-2 grid place-content-center text-neutral-3">
                                <ChevronRight height="1rem" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  );
};
