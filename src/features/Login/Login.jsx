import React from 'react';
import gambar from '../../assets/bg.png';
import { useDispatch, useSelector } from "react-redux";
import { LoginGoogle, LoginEmail } from "./../../config/redux/actions/authAction";
import { useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";


export default function Login() {
	const { dataLogin } = useSelector((state) => state.auth);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isLoading, setLoading] = useState(false);
	const navigate = useNavigate();
	const dispatch = useDispatch();

	const handleLoginGoogle = async (e) => {
		e.preventDefault();
		setLoading(true)
		const data = await dispatch(LoginGoogle());
		if (data) navigate("/search-car");
		setLoading(false);
	};
	
	const handleLoginEmail = async (e) => {
		e.preventDefault();
		setLoading(true)
		const data = await dispatch(LoginEmail(email, password));
		if (data) navigate("/dashboard");
		setLoading(false);
	};
	
	useEffect(() => {
		console.log(dataLogin);
		if (dataLogin?.email === "admin@admin.com") navigate("/dashboard");
		if (dataLogin?.email !== "admin@admin.com" && dataLogin !== null)
		  navigate("/search-car");
		// eslint-disable-next-line
	}, []);

	return (
		<div>
			<div className="flex w-screen h-screen">
			<img className="flex-grow object-cover" src={gambar} alt="Background"  />
			<div className="w-1/3 p-10 flex items-center justify-center">
				<div className="w-full flex flex-col gap-y-5">
					<div className="bg-gray-300 w-24 h-8"></div>
					<p className="font-bold text-2xl">Welcome in Binar Car Rental</p>
					<div id="alert" className="text-sm w-full mt-2 bg-pink text-red rounded-md py-4 px-8 bg-red bg-opacity-10 hidden"></div>
					{/* form sign-in */}
					<form className="flex flex-col font-light" id="sign-in-form">
						<p>Email</p>
						<input type="email" id="email" placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)} className="font-md w-full mt-2 mb-4 border border-gray-300 rounded-md p-2 outline-none" />
						<p>Password</p>
						<input type="password" id="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} className="text-sm w-full mt-2 mb-7 border border-gray-300 rounded-md p-2 outline-none" />
						<button onClick={handleLoginEmail} className="bg-primary text-white font-bold py-2 px-4 rounded-md">{isLoading ? 'Loading...' : 'Sign In'}</button>
						<button onClick={handleLoginGoogle} className="border border-primary text-primary font-bold py-2 px-4 mt-4 rounded-md">Sign In with Google</button>
					</form>
					</div>
				</div>
			</div>
		</div>
	);
};
