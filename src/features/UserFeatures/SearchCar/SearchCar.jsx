import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import Navbar from '../../../components/UserComponents/Navbar';
import Hero from '../../../components/UserComponents/Hero';
import CarInput from '../../../components/UserComponents/CarInput';
import Footer from '../../../components/UserComponents/Footer';
import Card from '../../../components/UserComponents/Card';

export default function SearchCar() {

    const [ isClickSearch, setIsClickSearch ] = useState(false);

    const clickSearch = () => {
		setIsClickSearch(true)
    };

    const { cars } = useSelector((globalStore) => globalStore.dataReducer);

    return (
        <div className='flex flex-col justify-center items-center'>
            <Navbar />
            <Hero />
            <CarInput clickSearch={clickSearch} isEdit={isClickSearch} />
            <div className="grid grid-cols-3 gap-5 w-full max-w-5xl">
                {
                    isClickSearch && cars.map(car => <Card key={car.id} car={car} />)
                }
            </div>
            <Footer />
        </div>
    )
}