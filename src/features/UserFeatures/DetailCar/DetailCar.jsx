import React from 'react';
import Navbar from '../../../components/UserComponents/Navbar';
import Hero from '../../../components/UserComponents/Hero';
import CarInput from '../../../components/UserComponents/CarInput';
import Detail from '../../../components/UserComponents/Detail';
import Footer from '../../../components/UserComponents/Footer';

export default function DetailCar() {

    return (
        <div className='flex flex-col justify-center items-center'>
            <Navbar />
            <Hero />
            <CarInput getData={() => {}} isDisabled={true} />
            <Detail />
            <Footer />
        </div>
    )
}