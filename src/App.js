import "./App.css";
import RouteApp from "./routes";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {
return <>
		<RouteApp />
		<ToastContainer />
	</>
}

export default App;
