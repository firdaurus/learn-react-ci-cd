import React from 'react';
import { Home, Truck } from 'react-feather';
import { Link } from 'react-router-dom';

export default function navbarBlue() {

  const route = window.location.pathname.replace('/','')

  return (
    <div className="bg-darkBlue-4 h-screen max-w-max flex flex-col items-center">
        <div className="bg-darkBlue-1 w-9 h-9 mt-4 flex-shrink-0"></div>
        <div className="flex flex-col mt-5 text-white">
            <Link to="/dashboard" className={`flex flex-col items-center py-3 px-1 gap-2 ${route === 'dashboard' && 'bg-white bg-opacity-30'}`}>
                <Home />
                <p className={`text-sm ${route === 'dashboard' && 'font-bold'}`}>Dashboard</p>
            </Link>
            <Link to="/list-car" className={`flex flex-col items-center py-3 px-1 gap-2 ${route === 'list-car' && 'bg-white bg-opacity-30'}`}>
                <Truck />
                <p className={`text-sm ${route === 'list-car' && 'font-bold'}`}>Cars</p>
            </Link>
        </div>
    </div>
  );
};
