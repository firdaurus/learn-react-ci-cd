import React from 'react'

export default function alert() {
  return (
    <div>
        {/* <!-- save alert --> */}
        <div id="alert-save-success" class="absolute mt-10 left-1/2 -translate-x-1/2 px-32 py-2 top-0 bg-success text-white text-base font-medium text-center rounded-sm hidden">
            Data Berhasil Disimpan
        </div>

        {/* <!-- delete alert --> */}
        <div id="alert-delete-success" class="absolute mt-10 left-1/2 -translate-x-1/2 px-32 py-2 top-0 bg-black text-white text-base font-medium text-center rounded-sm hidden">
            Data Berhasil Dihapus
        </div>
    </div>
  )
}
