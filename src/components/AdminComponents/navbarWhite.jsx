import React from 'react'

export default function navbarWhite() {

  const route = "dashboard"

  return (
    <div className="flex flex-col gap-6 pt-11 w-1/5 font-bold bg-white">
        <span className="text-neutral-3 pl-4">{route === 'dashboard' ? 'DASHBOARD' : 'CARS'}</span>
        <span className="bg-darkBlue-1 pl-4 py-3">{route === 'dashboard' ? 'Dashboard' : 'List Car'}</span>
    </div>
  )
}
