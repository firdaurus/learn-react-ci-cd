import React, {useState} from 'react';
import beepBeep from '../../assets/beepBeep.png';
import { toast } from 'react-toastify';

export default function DeletePopUp(props) {

    const [isLoading, setLoading] = useState(false);

    const handleDeleteCar = () => {
        setLoading(true);
        setTimeout(() => {
            //TODO: Add delete car API
            setLoading(false);
            props.onClose();
            toast.success("Mobil berhasil dihapus!", {
				position: "top-center",
				autoClose: 2000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true,
				progress: undefined,
            })
        }, 2000);
    }

  return (
    <div id="popup" className={`fixed w-screen h-screen bg-black bg-opacity-70 z-50 ${props.show ? 'flex' : 'hidden'} justify-center items-center`}>
        <div className="flex flex-col gap-6 justify-center items-center bg-white rounded-md px-8 py-5 w-96 text-center">
            <img src={beepBeep} alt="" className="w-28" />
            <p className="text-md font-bold">Menghapus Data Mobil</p>
            <p className="font-light">Setelah dihapus, data mobil tidak dapat dikembalikan. Yakin ingin menghapus?</p>
            <div className="grid grid-cols-2 justify-around gap-3">
                <button onClick={handleDeleteCar} className="bg-primary text-white font-bold py-2 px-4 rounded-sm">{isLoading ? 'Menghapus...' : 'Ya'}</button>
                <button onClick={props.onClose} className="border border-primary text-primary font-bold py-2 px-4 rounded-sm">Tidak</button>
            </div>
        </div>
    </div>
  )
}
