import React from 'react';
import { Search, ChevronDown  } from 'react-feather';

export default function header() {
  return (
    <div className="flex justify-between items-center bg-white py-4 shadow-md w-full px-5 z-10">
        <div className="bg-darkBlue-1 w-28 h-9"></div>
        <div className="flex gap-3 items-center">
            <div className="flex">
            {/* <!-- search --> */}
            <form action="" className="relative flex">
                <Search stroke-width="3" className="text-neutral-2 text-sm absolute left-2 top-1.5" />
                <input type="text" className="pl-10 pr-1 py-1.5 focus:outline-none border border-neutral-2 placeholder:font-light" placeholder="Search"/>
                <button type="submit" className="px-3 py-1 border border-primary text-primary text-lg font-bold" >Search</button>
            </form>
            </div>
            <div className="text-lg font-bold bg-darkBlue-1 w-9 h-9 rounded-full flex justify-center items-center">
            {/* <!-- username --> */}
            <span>A</span>
            </div>
            <span>Admin</span>
            <ChevronDown />
        </div>
    </div>
  )
}
