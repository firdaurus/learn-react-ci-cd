import React from "react";
import gambarMobil from "../../assets/car1.png";

export default function Hero() {
	return (
		<div className="bg-slate flex items-center justify-between container-lg px-24 pt-40 pb-32 w-screen overflow-x-hidden relative">
			<div className="flex flex-col gap-6">
				<p className="font-bold text-4xl w-1/2">Sewa &amp; Rental Mobil Terbaik di kawasan (Lokasimu)</p>
				<p className="font-light w-5/12">Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
			</div>
			<img 
                src={gambarMobil} 
                alt="Gambar Mobil" 
                className="absolute bottom-0 right-0 max-w-2xl"
            />
		</div>
	);
}