/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { searchCarAction } from '../../config/redux/actions/dataAction';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import { Calendar } from 'react-feather';

export default function CarInput({clickSearch, isEdit, isDisabled}) {

	const [date, setDate] = useState(null);
	const dispatch = useDispatch();

	const handleCalendarClose = () => console.log("Calendar closed");
	const handleCalendarOpen = () => console.log("Calendar opened");

	const submitSearch = () => {
		clickSearch();
		dispatch(searchCarAction());
	}

	return (
		<div className='flex flex-col gap-3 text-sm font-light bg-white p-5 w-full mx-auto max-w-screen-lg rounded-md relative transform -translate-y-1/2 shadow-md'>
			<p className={`font-bold ${isEdit || isDisabled ? '' : 'hidden' }`}>Pencarianmu</p>
			<div className='flex items-center justify-around gap-3'>
				<div className={`flex flex-col gap-2 font-light text-sm ${isDisabled ? 'w-1/4' : 'w-1/5'}`}>
					<label className='text-neutral-4'>Tipe Driver</label>
					<select 
					disabled={isDisabled}
					placeholder='Pilih Tipe Driver' 
					className={`text-neutral-3 border border-neutral-4 rounded-sm p-2 disabled:bg-neutral-3 disabled:opacity-100 disabled:border-none`} >
						<option disabled>Pilih Tipe Driver</option>
						<option>Dengan Sopir</option> 
						<option>Tanpa Sopir (Lepas Kunci)</option>
					</select>
				</div>
				<div  className={`flex flex-col gap-2 font-light text-sm ${isDisabled ? 'w-1/4' : 'w-1/5'}`}>
					<label className='text-neutral-4'>Tanggal</label>
					<div className='flex items-center justify-end relative'>
						<Calendar className='absolute text-neutral-3 right-2' height='1rem ' />
						<DatePicker
							disabled={isDisabled}
							dateFormat="dd/MM/yyyy"
							selected={date}
							onChange={(date) => setDate(date)}
							onCalendarClose={handleCalendarClose}
							onCalendarOpen={handleCalendarOpen}
							placeholderText="Pilih Tanggal"
							className={`text-neutral-3 border border-neutral-4 rounded-sm p-2 w-full disabled:bg-neutral-3 disabled:border-none placeholder:text-neutral-3`}
						/>
					</div>
				</div>
				<div className={`flex flex-col gap-2 font-light text-sm ${isDisabled ? 'w-1/4' : 'w-1/5'}`}>
					<label className='text-neutral-4'>Waktu Jemput/Ambil</label>
					<input 
						type='time'
						disabled={isDisabled}
						className={`text-neutral-3 border border-neutral-4 rounded-sm p-2 disabled:bg-neutral-3 disabled:border-none`} 
					/>
				</div>
				<div className={`flex flex-col gap-2 font-light text-sm ${isDisabled ? 'w-1/4' : 'w-1/5'}`}>
					<label className='text-neutral-4'>Jumlah Penumpang (opsional)</label>
					<input 
						type='number'
						disabled={isDisabled}
						placeholder='Jumlah Penumpang'
						className={`text-neutral-3 border border-neutral-4 rounded-sm p-2 disabled:bg-neutral-3 disabled:border-none placeholder:text-neutral-3`}
					/>
				</div>
				<button 
					onClick={submitSearch}
					className={`text-center self-end mt-5 py-2 px-3 font-bold rounded-sm ${isDisabled ? 'hidden' : ''} ${isEdit ? 'bg-white border border-darkBlue-4 text-darkBlue-4' : 'bg-limeGreen-4 border-none text-white'}`}
				>{isEdit ? 'Edit' : 'Cari Mobil'}</button>
			</div>
		</div>
	)
}