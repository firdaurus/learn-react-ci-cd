import React from 'react'

import iconFacebook from '../../assets/icon_facebook.png';
import iconInstagram from '../../assets/icon_instagram.png';
import iconTwitter from '../../assets/icon_twitter.png';
import iconMail from '../../assets/icon_mail.png';
import iconTwitch from '../../assets/icon_twitch.png';

export default function Footer() {
    return (
        <div className='grid grid-cols-7 gap-20 justify-center px-32 py-24'>
            <div className='flex flex-col gap-3 font-light col-span-2'>
                <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                <p>binarcarrental@gmail.com</p>
                <p>081-233-334-808</p>
            </div>
            <div className='flex flex-col gap-3 col-span-1'>
                <p>Our services</p>
                <p>Why Us</p>
                <p>Testimonial</p>
                <p>FAQ</p>
            </div>
            <div className='flex flex-col gap-3 font-light col-span-2'>
                <p>Connect with us</p>
                <div className='flex gap-2'>
                    <img src={iconFacebook} alt='icon facebbook'/>
                    <img src={iconInstagram} alt='icon instagram'/>
                    <img src={iconTwitter} alt='icon twitter'/>
                    <img src={iconMail} alt='icon mail'/>
                    <img src={iconTwitch} alt='icon twitch'/>
                </div>
            </div>
            <div className='flex flex-col gap-3 font-light col-span-2'>
                <p>Copyright Binar 2022</p>
                <div className='bg-darkBlue-4 w-24 h-8'></div>
            </div>
        </div>
    )
}