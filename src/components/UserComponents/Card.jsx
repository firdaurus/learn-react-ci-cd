/* eslint-disable react/prop-types */
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { Users, Settings, Calendar } from 'react-feather';
import { selectedCarAction } from '../../config/redux/actions/dataAction';
import { useDispatch } from 'react-redux';

//TODO: rapikan

export default function Card({ car }) {

	const navigate = useNavigate()
	const dispatch = useDispatch();

	const handleClick = () => {
		dispatch(selectedCarAction(car))
		return navigate('/detail-car')
	}
	
	const formatRupiah = (angka) => "Rp" + angka.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

    return (
		<div className="flex flex-col items-center justify-between rounded-md border border-neutral-100 shadow-md p-5">
			<div className="flex flex-col gap-3">
				<img 
					src={car.image} 
					alt={`Gambar ${car.name}`}
					className='h-32 self-center scale-x-[-1]'	
				/>
				<p>{car.name}</p>
				<p className="font-bold">{formatRupiah(car.price)} / hari</p>
				<p className='font-light'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
				<div className="flex gap-1 items-center font-light">
					<Users height="1rem" className="text-neutral-3" />
					<span>{car.passenger}</span>
				</div>
				<div className="flex gap-1 items-center font-light">
					<Settings height="1rem" className="text-neutral-3" />
					<span>{car.engine}</span>
				</div>
				<div className="flex gap-1 items-center font-light">
					<Calendar height="1rem" className="text-neutral-3" />
					<span>{car.year}</span>
				</div>
			</div>
			<button onClick={handleClick} className="text-center self-end mt-5 py-2 font-bold text-white bg-limeGreen-4 rounded-sm w-full">Pilih Mobil</button>
		</div>
    )
}