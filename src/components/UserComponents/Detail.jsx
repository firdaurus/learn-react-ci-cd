/* eslint-disable react/prop-types */
import React from 'react'
import { Users, Settings, Calendar, ChevronUp } from 'react-feather';
import { useSelector } from 'react-redux';

export default function Detail() {
    const { selectedCar } = useSelector((globalStore) => globalStore.dataReducer);

    const formatRupiah = (angka) => "Rp" + angka.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

    return (
        <div className='flex justify-between gap-5 w-full max-w-5xl'>
            <div className='w-2/3 flex flex-col'>
                <div className='bg-white rounded-md border border-neutral-100 shadow-md p-8'>
                    <p className='font-bold'>Tentang Paket</p>
                    <p className='font-light mt-4 mb-2'>Include</p>
                    <ul className='list-disc font-light text-neutral-3 ml-8'>
                        <li>Apa saja yang termasuk dalam paket misal durasi max 12 jam</li>
                        <li>Sudah termasuk bensin selama 12 jam</li>
                        <li>Sudah termasuk Tiket Wisata</li>
                        <li>Sudah termasuk pajak</li>
                    </ul>
                    <p className='font-light mt-4 mb-2'>Exclude</p>
                    <ul className='list-disc font-light text-neutral-3 ml-8'>
                        <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                        <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                        <li>Tidak termasuk akomodasi penginapan</li>
                    </ul>
                    <div className='flex justify-between items-center'>
                        <p className='font-bold my-4'>Refund, Reschedule, Overtime</p>
                        <ChevronUp height="2rem"/>
                    </div>
                    <ul className='list-disc font-light text-neutral-3 ml-8'>
                        <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                        <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                        <li>Tidak termasuk akomodasi penginapan</li>
                        <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                        <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                        <li>Tidak termasuk akomodasi penginapan</li>
                        <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                        <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                        <li>Tidak termasuk akomodasi penginapan</li>
                    </ul>
                </div>
                <button className="text-center self-end mt-5 py-2 font-bold text-white bg-limeGreen-4 rounded-sm w-1/3">Lanjutkan Pembayaran</button>
            </div>
            <div className="flex flex-col rounded-md border border-neutral-100 shadow-md p-8 w-1/3 h-fit">
                <div className="flex flex-col gap-3">
                    <img 
                        src={selectedCar.image} 
                        alt="Gambar mobil"
                        className='h-32 self-center scale-x-[-1]'	
                    />
                    <p>{selectedCar.name}</p>
                    <div className='flex flex-row gap-2 text-neutral-3 font-light text-xs'>
                        <div className="flex gap-1 items-center">
                            <Users height="1rem" />
                            <span>{selectedCar.passenger}</span>
                        </div>
                        <div className="flex gap-1 items-center">
                            <Settings height="1rem" />
                            <span>{selectedCar.engine}</span>
                        </div>
                        <div className="flex gap-1 items-center">
                            <Calendar height="1rem" />
                            <span>{selectedCar.year}</span>
                        </div>
                    </div>
                    <div className='flex items-center justify-between'>
                        <span>Total</span>
                        <span className="font-bold">{formatRupiah(selectedCar.price)}</span>
                    </div>
                </div>
                <button className="text-center self-end mt-5 py-2 font-bold text-white bg-limeGreen-4 rounded-sm w-full">Lanjutkan Pembayaran</button>
            </div>
        </div>
    )
}