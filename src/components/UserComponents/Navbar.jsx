import React from 'react';
import { useSelector } from 'react-redux';

export default function Navbar() {
    const { dataLogin } = useSelector((state) => state.auth);

    return (
        <div className='flex items-center justify-between container-lg px-24 pt-5 pb-5 bg-slate w-screen fixed top-0 left-0 right-0 shadow-sm z-50'>
            <div className='bg-darkBlue-4 w-24 h-8'></div>
            <div className='bg-slate flex items-center justify-center gap-5'>
                <span>Our Services</span>
                <span>Why Us</span>
                <span>Testimonial</span>
                <span>FAQ</span>
                <button className='bg-limeGreen-4 text-white font-bold rounded-sm px-2 py-1'>{dataLogin.user.displayName}</button>
            </div>
        </div>
    )
}