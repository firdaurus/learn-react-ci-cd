import { BrowserRouter, Routes, Route } from "react-router-dom";
import Register from "./features/Register/Register";
import Login from "./features/Login/Login";
import Dashboard from "./features/AdminFeatures/Dashboard/Dashboard";
import ListCar from "./features/AdminFeatures/ListCar/ListCar";
import SearchCar from "./features/UserFeatures/SearchCar/SearchCar";
import DetailCar from "./features/UserFeatures/DetailCar/DetailCar";
import AddCar from "./features/AdminFeatures/AddCar/AddCar";

const RouteApp = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Register />} />
        <Route path="/login" element={<Login />} />

        <Route path="/dashboard" element={<Dashboard />} />
        <Route path="/list-car" element={<ListCar />} />
        <Route path="/add-car" element={<AddCar />} />
        {/*<Route path="/cars/submit-car" element={<SubmitCar />} />
        <Route path="/cars/delete-car" element={<DeleteCar />} /> */}

        <Route path="/search-car" element={<SearchCar />} />
        <Route path="/detail-car" element={<DetailCar />} />
        <Route path="*" element={<h1>-404- Page Not Found</h1>} />
      </Routes>
    </BrowserRouter>
  );
};

export default RouteApp;