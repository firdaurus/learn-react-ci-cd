/* eslint-disable testing-library/no-render-in-setup */
import { render, screen } from '@testing-library/react';
import Login from '../features/Login/Login';
import { Provider } from "react-redux";
import { store, persistor } from "../config/redux";
import { PersistGate } from "redux-persist/integration/react";

beforeEach(() => render(
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <Login />
      </PersistGate>
    </Provider>
));

const mockedUsedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
   ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedUsedNavigate,
}));

test("Render login normally", () => {
    expect(screen.getByText("Sign In")).toBeInTheDocument();
})

test("Has email input", () => {
    expect(screen.getByPlaceholderText('Email')).toBeInTheDocument();
})

test("Has password input", () => {
    expect(screen.getByPlaceholderText('Password')).toBeInTheDocument();
})

test("Has submit button", () => {
    expect(screen.getByText("Sign In")).toBeInTheDocument();
})