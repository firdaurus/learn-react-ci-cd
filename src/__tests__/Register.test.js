/* eslint-disable testing-library/no-render-in-setup */
import { render, screen } from '@testing-library/react';
import Register from '../features/Register/Register';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from "react-redux";
import { store, persistor } from "../config/redux";
import { PersistGate } from "redux-persist/integration/react";

beforeEach(() => render(
    <Provider store={store}>
      <PersistGate persistor={persistor}>
          <Router>
            <Register />
          </Router>
      </PersistGate>
    </Provider>
));

const mockedUsedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
   ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedUsedNavigate,
  useHref: () => jest.fn(),
}));

test('render register page normally', () => {
    expect(screen.getByText('Sign Up')).toBeInTheDocument();
});

test('has email input', () => {
    expect(screen.getByPlaceholderText('Contoh: johndee@gmail.com')).toBeInTheDocument();
});

test('has password input', () => {
    expect(screen.getByPlaceholderText('6+ karakter')).toBeInTheDocument();
});

test('has submit button', () => {
    expect(screen.getByText('Sign Up')).toBeInTheDocument();
});